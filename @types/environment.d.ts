declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: "production" | "develop" | "test";
      DATABASE: string;
      SESSION_SECRET: string;
      PORT: string;
      FACEBOOK_CLIENT_ID: string;
      FACEBOOK_CLIENT_SECRET: string;
      FACEBOOK_CALLBACK_URL: string;
      GOOGLE_CLIENT_ID: string;
      GOOGLE_CLIENT_SECRET: string;
      GOOGLE_CALLBACK_URL: string;
    }
  }
}
export {};
