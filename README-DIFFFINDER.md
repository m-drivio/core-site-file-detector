# Diff Finder

This application helps us to compare two html files (Dynamic/static)

## How to Compare two html files?

- open site http://dashboard.drivio.net/
- Choose page name
- Select the file from your system (related to selected page name above)
- Hit `Find Difference`

1. `On detect changes`: It will shows up a modal report
1. `Other messages`: Shows the message above the form

### All pages

1. Home
1. Ticket not found
1. Confirmation
1. Maintenance
1. Ticket
1. Windshield
1. Checkout
1. Receipt

---

## Start development

---

1. `git clone https://maheshdrivio@bitbucket.org/finenyc/diff-finder.git`
1. `npm install`
1. `npm install -g diff2html-cli`
1. `npm install prettydiff -g`
1. `create uploads folder in root`
1. `create result folder in root`
1. `create temp folder in root`
1. `copy, rename and edit example.env file`
1. `npm start`

--- Done

---

## Server setup

---

See ## Start development (except `npm start`)

Folders to be ignored

1. `uploads` - Files will upload here
2. `result` - compare result file will be here
3. `temp` - temporery files will be here
4. `npm run tsc`

> Once build project, you can start project using pm2

---

## Application other details

---

`Visit`: https://www.npmjs.com/package/diff2html-cli

`Commands`: `diff2html --help` in terminal

Entry file: `root/src/controllers/TodoController.ts`

> Other files `root/src/services/`

```
├── get-content.ts
└── matcher
   ├── Extract.ts
   ├── checkout.ts
   ├── class.json
   ├── receipt.ts
   ├── tickets.ts
   └── windshield.ts
```

> Data folder container

```
├── checkout.html
├── confirmation.html
├── home.html
├── maintenance.html
├── receipt.html
├── templates
|  ├── blockedTicket.html
|  ├── notSelectedTicket.html
|  ├── notSelectedTicketViolation.html
|  ├── notSelectedTicketWindshield.html
|  ├── selectTicketWindshield.html
|  ├── selectedTicket.html
|  └── selectedTicketViolation.html
├── ticket.html
├── ticketNotFound.html
└── windshield.html
```

> =====Done====
