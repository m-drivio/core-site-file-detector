"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.beforeAllRoute = void 0;
/**
 * This Middleware executes before all Routes
 * Use this middleware to set data based on requirement
 *
 * Below I have set isAuthenticated data so it can be accessed
 * in template files to hide and show login button
 */
exports.beforeAllRoute = function (req, res, next) {
    res.locals.isAuthenticated = req.isAuthenticated();
    next();
};
//# sourceMappingURL=beforeAllRoute.js.map