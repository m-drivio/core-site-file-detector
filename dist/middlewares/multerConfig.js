"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.upload = void 0;
const multer_1 = __importDefault(require("multer"));
exports.upload = multer_1.default({
    limits: {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        fileSize: +process.env.FILE_SIZE,
    },
    fileFilter(req, file, cb) {
        // if (
        //   !file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|svg|SVG|pdf|PDF)$/)
        // ) {
        //   return cb(new Error("Please upload an image."));
        // }
        cb(null, true);
    },
    storage: multer_1.default.diskStorage({
        destination: function (req, file, cb) {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            cb(null, process.env.DEST_FOLDER);
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        },
    }),
});
//# sourceMappingURL=multerConfig.js.map