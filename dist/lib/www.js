"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.runServer = void 0;
const http_1 = __importDefault(require("http"));
const debug = require("debug")("app");
/// Function to run server
function runServer(app, PORT) {
    debug("Server configuration started.");
    /// Exit process if invalid PORT
    if (PORT <= 0) {
        console.error("Port number must be number and > 0");
        return process.exit(1);
    }
    console.log("Running on port", PORT, `http://localhost:${PORT}`);
    /// set port to app
    app.set("port", PORT);
    /// create server
    const server = http_1.default.createServer(app);
    server.listen(PORT);
    /// Success listener
    server.on("listening", function onListening() {
        const addr = server.address();
        const bind = typeof addr === "string" ? "pipe " + addr : "port " + (addr === null || addr === void 0 ? void 0 : addr.port);
        debug("Listening on " + bind);
    });
    /// Error Listener
    server.on("error", function onError(error) {
        if (error.syscall !== "listen") {
            throw error;
        }
        /// handle specific listen errors with friendly messages
        switch (error.code) {
            case "EACCES":
                console.error(" requires elevated privileges");
                process.exit(1);
                break;
            case "EADDRINUSE":
                console.error(PORT.toString() + " is already in use");
                process.exit(1);
                break;
            default:
                throw error;
        }
    });
}
exports.runServer = runServer;
//# sourceMappingURL=www.js.map