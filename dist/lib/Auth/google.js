"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setStrategy = exports.setRoute = void 0;
const passport_1 = __importDefault(require("passport"));
const passport_google_oauth_1 = __importDefault(require("passport-google-oauth"));
const verifyUser_1 = require("./verifyUser");
/// Set Route
function setRoute(app) {
    app.get("/auth/google", passport_1.default.authenticate("google", {
        scope: ["https://www.googleapis.com/auth/plus.login"],
    }));
    app.get("/callback/google", passport_1.default.authenticate("google", {
        successRedirect: "/",
        failureRedirect: "/login",
    }));
}
exports.setRoute = setRoute;
/// Login Strategy
function setStrategy(clientID, clientSecret, callbackURL) {
    passport_1.default.use(new passport_google_oauth_1.default.OAuth2Strategy({
        clientID,
        clientSecret,
        callbackURL,
    }, verifyUser_1.verifyUser));
}
exports.setStrategy = setStrategy;
//# sourceMappingURL=google.js.map