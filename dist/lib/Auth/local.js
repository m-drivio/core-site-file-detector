"use strict";
/**
 * Authenticate request using passport's local strategy
 *
 * @packageDocumentation
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.local = void 0;
const passport_1 = __importDefault(require("passport"));
const models_1 = require("../../models");
/// This package is handling Passport Local Strategy
/// https://www.npmjs.com/package/passport-local-mongoose
function local() {
    passport_1.default.use(models_1.User.createStrategy());
    passport_1.default.serializeUser(models_1.User.serializeUser());
    passport_1.default.deserializeUser(models_1.User.deserializeUser());
}
exports.local = local;
//# sourceMappingURL=local.js.map