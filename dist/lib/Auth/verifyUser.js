"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyUser = void 0;
const debug = require("debug")("Passport:Facebook");
const models_1 = require("../../models");
/// Facebook and Google Login Callback
/// Provider Login callback
exports.verifyUser = async (accessToken, refreshToken, profile, done) => {
    /// Find or Create user
    try {
        const user = await models_1.User.findOne({ "profile.id": profile.id });
        /// call callback if user has already in database
        if (user)
            return done(null, user);
        /// No user exists, so creating new user with facebook id
        const newUser = await models_1.User.create({
            username: profile.id,
            profile: {
                id: profile.id,
                access_token: accessToken,
                refresh_token: refreshToken,
                provider: profile.provider,
                emails: profile.emails,
            },
        });
        done(null, newUser);
    }
    catch (error) {
        done(error);
    }
};
//# sourceMappingURL=verifyUser.js.map