"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setStrategy = exports.setRoute = void 0;
const passport_1 = __importDefault(require("passport"));
const passport_facebook_1 = __importDefault(require("passport-facebook"));
const verifyUser_1 = require("./verifyUser");
/// Set Route
function setRoute(app) {
    app.get("/auth/facebook", passport_1.default.authenticate("facebook", {
        scope: "email",
    }));
    app.get("/callback/facebook", passport_1.default.authenticate("facebook", {
        successRedirect: "/",
        failureRedirect: "/login",
    }));
}
exports.setRoute = setRoute;
/// Facbook strategy
function setStrategy(clientID, clientSecret, callbackURL) {
    passport_1.default.use(new passport_facebook_1.default.Strategy({
        clientID,
        clientSecret,
        callbackURL,
    }, verifyUser_1.verifyUser));
}
exports.setStrategy = setStrategy;
//# sourceMappingURL=facebook.js.map