"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const bluebird_1 = __importDefault(require("bluebird"));
function default_1(mongodbDatabaseUrl, NODE_ENV) {
    /// Set promise
    mongoose_1.default.Promise = bluebird_1.default;
    /// Append _test when testing
    if (NODE_ENV === "test") {
        mongodbDatabaseUrl += "_test";
    }
    /// Connect and Return promise
    return mongoose_1.default.connect(mongodbDatabaseUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    });
}
exports.default = default_1;
//# sourceMappingURL=_database.js.map