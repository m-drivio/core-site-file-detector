"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_session_1 = __importDefault(require("express-session"));
const connect_flash_1 = __importDefault(require("connect-flash"));
const MongoStore = require("connect-mongo")(express_session_1.default);
const mongoose = require("mongoose"); //intentionally required
/// Use session middleware
/// Use flash middleware
function default_1(app, secret) {
    /// Session option
    const options = {
        secret: secret,
        resave: false,
        saveUninitialized: true,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24,
        },
        store: new MongoStore({
            mongooseConnection: mongoose.connection,
        }),
    };
    /// Only Production options
    if (app.get("env") === "production") {
        app.set("trust proxy", 1);
        options.cookie = {
            secure: true,
        };
    }
    /// add to app
    app.use(express_session_1.default(options));
    app.use(connect_flash_1.default());
}
exports.default = default_1;
//# sourceMappingURL=_session.js.map