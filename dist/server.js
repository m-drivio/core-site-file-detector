"use strict";
/**
 * Main file to configure and run express server
 *
 * @packageDocumentation
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const express_1 = require("@softcripto/express");
const www_1 = require("./lib/www");
const _database_1 = __importDefault(require("./lib/_database"));
const _session_1 = __importDefault(require("./lib/_session"));
const cors_1 = __importDefault(require("cors"));
const debug = require("debug")("app");
const robots = require("express-robots-txt");
// import * as auth from "./auth";
// load all controllers that handles http request
require("./controllers");
debug("Database connection started.");
/*
 * Connecting to database
 * When connected configure server
 */
_database_1.default(process.env.DATABASE, process.env.NODE_ENV)
    .then(() => {
    debug("Database connection success.");
    // Initialising Express App with @softcripto/express
    const app = express_1.Server.create({
        views: "views",
        public: "public",
        // Run code just before Route mount
        beforeRouteInjection: function (app) {
            app.use(cors_1.default());
            if (process.env.NODE_ENV != "production") {
                app.use(robots({ UserAgent: "*", Disallow: "/" }));
            }
            // Set session and flash middleware
            _session_1.default(app, process.env.SESSION_SECRET);
            /*
             * Remove when auth required
             * // enable auth
             * auth.enableAuth(app);
             */
        },
    });
    // Run server
    www_1.runServer(app, process.env.PORT);
})
    .catch((err) => {
    console.log(err);
    process.exit(1); // Doesn't run server
});
//# sourceMappingURL=server.js.map