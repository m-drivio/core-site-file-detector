"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Class: CompareHtmlElementNode
 * It helps to compare html element's `name`, `type` and `node text`
 *
 * @example <caption>How to use? </caption>
 * ```
 * import compareHtmlElementNodes from 'path/compare-html-element-node.ts';
 * let compareNodeElement = new compareHtmlElementNodes(fileOne, fileTwo);
 * let result = compareNodeElement.startCompare();
 * ```
 */
class CompareHtmlElementNode {
    /**
     *
     * @param fileOne -  first  html file cheerio object
     * @param fileTwo - second html file cheerio object
     *
     * @example <caption>Param object example </caption>
     * ```
     * [
        {
          type: 'script',
          name: 'script',
          namespace: 'http://www.w3.org/1999/xhtml',
          attribs: [Object: null prototype] {
            src: './public/js/jquery-1.11.3.min.js'
          },
          'x-attribsNamespace': [Object: null prototype] { src: undefined },
          'x-attribsPrefix': [Object: null prototype] { src: undefined },
          children: [],
          parent: {
            type: 'tag',
            name: 'body',
            namespace: 'http://www.w3.org/1999/xhtml',
            attribs: [Object: null prototype],
            'x-attribsNamespace': [Object: null prototype],
            'x-attribsPrefix': [Object: null prototype],
            children: [Array],
            parent: [Object],
            prev: [Object],
            next: null
          },
    ]
     * ```
     */
    constructor(fileOne, fileTwo) {
        this.fileOne = fileOne;
        this.fileTwo = fileTwo;
        this.result = { status: "not matched" };
    }
    /**
     * This function starts the comparision between two file node elements
     */
    startCompare() {
        // compare element name
        if (this.fileOne.name !== this.fileTwo.name) {
            this.generateResult(this.fileOne.name, this.fileTwo.name);
            return this.result;
        }
        // Comparing element type
        if (this.fileOne.type !== this.fileTwo.type) {
            this.generateResult(this.fileOne.type, this.fileTwo.type);
            return this.result;
        }
        // Comparing node text
        if (this.fileOne.type === "text") {
            if (this.fileOne.data !== this.fileTwo.data) {
                this.generateResult(this.fileOne.data, this.fileTwo.data);
                return this.result;
            }
        }
        this.result.status = "matched";
        return this.result;
    }
    /**
     * @param data1  - name|type|name of element of the first file
     * @param data2  - name|type|name of element of the second file
     *
     * @exclude
     */
    generateResult(data1, data2) {
        this.result["report"] = `Not matched '${data1}' with '${data2}'`;
    }
}
exports.default = CompareHtmlElementNode;
//# sourceMappingURL=compare-html-element-node.js.map