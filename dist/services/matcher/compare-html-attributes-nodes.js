"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * # compareHtmlAttributesNodes
 * ### Compares html element's  key & value of attributes
 *
 * @param firstAttrs -  cheerio attributes object for first file
 * @param secondAttrs - cheerio attributes object for second file
 *
 * @example <caption> How to use </caption>
 * ```
 * compareHtmlAttributesNodes(attr1, attr2);
 * ```
 */
function compareHtmlAttributesNodes(elementOne, elementTwo) {
    const result = { status: "not matched" };
    const hasFileOneAttrs = elementOne.hasOwnProperty("attribs");
    const hasFileTwoAttrs = elementTwo.hasOwnProperty("attribs");
    // if file one doesn't have attributes (FAIL)
    if (!hasFileOneAttrs && hasFileTwoAttrs) {
        result["report"] = "File one doesn't have attribute";
        return result;
    }
    // if file two doesn't have attributes (FAIL)
    if (hasFileOneAttrs && !hasFileTwoAttrs) {
        result["report"] = "File two doesn't have attribute";
        return result;
    }
    // If both file doesn't have attribute (PASS)
    if (!hasFileOneAttrs && !hasFileTwoAttrs) {
        result.status = "matched";
        result["report"] = "Both element does't have attributes";
        return result;
    }
    const firstAttrs = elementOne.attribs;
    const secondAttrs = elementTwo.attribs;
    const keys1 = Object.keys(firstAttrs);
    const keys2 = Object.keys(secondAttrs);
    let i = 0;
    const keys1length = keys1.length;
    for (i; i < keys1length; i++) {
        const key1 = keys1[i];
        const key2 = keys2[i];
        const val1 = firstAttrs[key1];
        const val2 = secondAttrs[key2];
        // console.log(key1, key2);
        if (key1 !== key2) {
            result["report"] = `Attr for '${keys1[i]}'`;
            return result;
        }
        if (val1 !== val2) {
            result["report"] = `'${val1}' for attr '${key1}'`;
            return result;
        }
    }
    return { status: "matched" };
}
exports.default = compareHtmlAttributesNodes;
//# sourceMappingURL=compare-html-attributes-nodes.js.map