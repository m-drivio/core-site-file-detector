"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
/**
 * get content
 * this function helps to get html content based on file path
 *
 * @param filePath - file directory path
 */
function getContent(filePath) {
    const htmlDocument = fs_1.default.readFileSync(path_1.default.resolve(filePath), {
        encoding: "utf8",
        flag: "r",
    });
    return removeSpaces(htmlDocument);
    // return cheerio.load(removeSpaces(htmlDocument));
}
exports.default = getContent;
function removeSpaces(string) {
    return string
        .replace(/\n/g, "")
        .replace(/[\t ]+\</g, "<")
        .replace(/\>[\t ]+\</g, "><")
        .replace(/\>[\t ]+$/g, ">");
}
//# sourceMappingURL=get-content.js.map