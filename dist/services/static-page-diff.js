"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/* eslint-disable no-prototype-builtins */
const compare_html_attributes_nodes_1 = __importDefault(require("../services/matcher/compare-html-attributes-nodes"));
const compare_html_element_node_1 = __importDefault(require("./matcher/compare-html-element-node"));
// let result = compareHtmlAttributesNodes(attr1, attr2);
// console.log("Result", result);
/**
 * Class: HtmlDiff
 */
class HtmlDiff {
    constructor(fileA, fileB) {
        this.storeA = fileA;
        this.storeB = fileB;
        this.timer = 0;
        this.result = {
            status: "completed",
            error: 0,
        };
    }
    startCompare() {
        this.timer++;
        if (this.storeA.length == 0) {
            this.result["status"] = "completed";
            return;
        }
        const itemA = this.storeA[0];
        const itemB = this.storeB[0];
        const hasChildOfItemA = itemA.hasOwnProperty("children");
        const hasChildOfItemB = itemB.hasOwnProperty("children");
        // =======Compare elements nodes ====//
        const compareElementNode = new compare_html_element_node_1.default("itemA", "itemB");
        const elementResult = compareElementNode.startCompare();
        if (elementResult.status != "matched") {
            this.result["error"] = 1;
            this.result["report"] = elementResult.report;
            return;
        }
        const attributeResult = compare_html_attributes_nodes_1.default(itemA, itemB); // compare attributes
        if (attributeResult.status != "matched") {
            this.result["error"] = 1;
            this.result["report"] = attributeResult.report;
            return;
        }
        // all other testings goes here ...
        this.storeA.splice(0, 1);
        this.storeB.splice(0, 1);
        if (hasChildOfItemA && itemA.children.length > 0) {
            this.updateStoreA(itemA.children);
        }
        if (hasChildOfItemB && itemB.children.length > 0) {
            this.updateStoreB(itemB.children);
        }
        return this.startCompare();
    }
    /**
     * Push children data
     * since, we have multiple children array even inside one element object
     * so, we need to loop through the array and update the store
     * @param {Array} children
     */
    updateStoreA(children) {
        let i = 0;
        const childrenLength = children.length;
        for (i; i < childrenLength; i++) {
            this.storeA.push(children[i]);
        }
    }
    /**
     * Push children data
     * since, we have multiple children array even inside one element object
     * so, we need to loop through the array and update the store
     * @param {Array} children
     */
    updateStoreB(children) {
        let i = 0;
        const childrenLength = children.length;
        for (i; i < childrenLength; i++) {
            this.storeB.push(children[i]);
        }
    }
}
exports.default = HtmlDiff;
//# sourceMappingURL=static-page-diff.js.map