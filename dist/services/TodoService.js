"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodoService = void 0;
/// Todo Service for Template
class TodoService {
    constructor(todo) {
        this.id = todo._id;
        this.title = todo.title;
        this.status = todo.hasCompleted ? "Completed" : "Pending";
        this.createdAt = todo.createdAt.toDateString();
    }
    static init(todos) {
        const list = [];
        todos.forEach((todo) => {
            list.push(new TodoService(todo));
        });
        return list;
    }
}
exports.TodoService = TodoService;
//# sourceMappingURL=TodoService.js.map