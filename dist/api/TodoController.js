"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("@softcripto/express");
const models_1 = require("../models");
let APITodoController = class APITodoController {
    async index(req, res, next) {
        try {
            const todos = await models_1.Todo.find().sort("-createdAt");
            res.json(todos);
        }
        catch (error) {
            next(error);
        }
    }
    async create(req, res, next) {
        try {
            const todo = await models_1.Todo.create(req.body);
            res.json(todo);
        }
        catch (error) {
            next(error);
        }
    }
    update() { }
    remove() { }
};
APITodoController.children = []; /// required
__decorate([
    express_1.Get("/")
], APITodoController.prototype, "index", null);
__decorate([
    express_1.Post("/")
], APITodoController.prototype, "create", null);
APITodoController = __decorate([
    express_1.Controller("/api/todos")
], APITodoController);
exports.default = APITodoController;
//# sourceMappingURL=TodoController.js.map