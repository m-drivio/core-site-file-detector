"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("@softcripto/express");
const models_1 = require("../models");
// import { TodoService } from "../services/TodoService";
const multerConfig_1 = require("../middlewares/multerConfig");
// import { ensureLoggedIn } from "connect-ensure-login";
/** New test*/
const { promisify } = require("util"), exec = promisify(require("child_process").exec);
// @Controller('/todos')
let TodoController = class TodoController {
    /**
     * List all todos.
     */
    async index(req, res, next) {
        const result = { error: 0 };
        console.log("PATH", req.file);
        const filePath = `data/${req.body.pageName}.html`;
        try {
            const command = "colordiff --suppress-common-lines -W 300 -w -y -b ";
            const cmd = command + filePath + " " + req.file.path;
            const { stdout, stderr } = await exec(cmd, { maxBuffer: 1024 * 1024 });
            result["data"] = stdout;
            res.send(result);
        }
        catch (error) {
            result["error"] = 1;
            result["data"] = error.stdout;
            console.log("ERR", error);
            res.send(result);
        }
    }
    /**
     * Render Form to create new todo.
     */
    async createForm(req, res, next) {
        try {
            res.locals.errorMessage = req.flash("error");
            res.render("todos/create");
        }
        catch (error) {
            next(error);
        }
    }
    /**
     * Handles New Todo
     *
     */
    async create(req, res, next) {
        try {
            const exists = await models_1.Todo.findOne({ title: req.body.title });
            if (exists) {
                req.flash("error", "Same title already exists.");
                res.redirect(req.originalUrl);
                return;
            }
            await models_1.Todo.create(req.body);
            req.flash("success", "New Task created successfully.");
            res.redirect("/todos");
        }
        catch (error) {
            next(error);
        }
    }
    async removeTodo(req, res, next) {
        try {
            const exists = await models_1.Todo.findById(req.params.id);
            if (!exists) {
                req.flash("error", "Invalid todo. Could not be removed.");
                res.redirect("/todos");
                return;
            }
            await models_1.Todo.remove({ _id: req.params.id });
            req.flash("success", "Task deleted succuessfully.");
            res.redirect("/todos");
        }
        catch (error) {
            next(error);
        }
    }
    /**
     * Update a Todo
     *
     */
    async updateForm(req, res, next) {
        try {
            const todo = await models_1.Todo.findById(req.params.id);
            if (!todo) {
                req.flash("error", "Invalid todo id.");
                res.redirect("/todos");
                return;
            }
            res.locals.todo = todo;
            res.render("todos/edit");
        }
        catch (error) {
            next(error);
        }
    }
    /**
     * Update
     */
    async update(req, res, next) {
        try {
            const todo = await models_1.Todo.findById(req.params.id);
            if (!todo) {
                req.flash("error", "Invalid todo id.");
                res.redirect("/todos");
                return;
            }
            await todo.update({ title: req.body.title });
            req.flash("success", "Task updated successfully.");
            res.redirect("/todos");
        }
        catch (error) {
            next(error);
        }
    }
};
TodoController.children = []; /// required
__decorate([
    express_1.Middleware(multerConfig_1.upload.single("coverImg")),
    express_1.Post("/")
], TodoController.prototype, "index", null);
__decorate([
    express_1.Get("/create")
], TodoController.prototype, "createForm", null);
__decorate([
    express_1.Post("/create")
], TodoController.prototype, "create", null);
__decorate([
    express_1.Get("/delete/:id")
], TodoController.prototype, "removeTodo", null);
__decorate([
    express_1.Get("/update/:id")
], TodoController.prototype, "updateForm", null);
__decorate([
    express_1.Post("/update/:id")
], TodoController.prototype, "update", null);
TodoController = __decorate([
    express_1.Controller("/todos")
], TodoController);
exports.default = TodoController;
//# sourceMappingURL=TodoController.js.map