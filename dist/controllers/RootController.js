"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("@softcripto/express");
const passport_1 = __importDefault(require("passport"));
let RootController = class RootController {
    /// Home page
    async index(req, res, next) {
        try {
            res.render("root/home");
        }
        catch (error) {
            next(error);
        }
    }
    /// Demo contact page
    one(req, res) {
        res.render("root/contact");
    }
    /// User Login where user can enter credentials
    /// Or Use third party login Facebook, Google etc.
    loginForm(req, res) {
        if (req.isAuthenticated()) {
            return res.redirect("/");
        }
        res.locals.message = req.query.info; /// query string
        res.render("user/login", { layout: "guest" });
    }
    /// Handle User Login submission
    /// Authenticate using local strategy
    login(req, res, next) {
        passport_1.default.authenticate("local", (err, user, info) => {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.redirect("/login?info=" + info);
            }
            req.logIn(user, function (err) {
                var _a;
                if (err) {
                    return next(err);
                }
                return res.redirect(((_a = req.session) === null || _a === void 0 ? void 0 : _a.returnTo) || "/");
            });
        })(req, res, next);
    }
    /// Logout user
    logout(req, res) {
        req.logOut();
        res.redirect("/");
    }
};
RootController.children = []; /// required
__decorate([
    express_1.Get("/")
], RootController.prototype, "index", null);
__decorate([
    express_1.Get("/contact")
], RootController.prototype, "one", null);
__decorate([
    express_1.Get("/login")
], RootController.prototype, "loginForm", null);
__decorate([
    express_1.Post("/login")
], RootController.prototype, "login", null);
__decorate([
    express_1.Get("/logout")
], RootController.prototype, "logout", null);
RootController = __decorate([
    express_1.Controller("/")
], RootController);
exports.default = RootController;
//# sourceMappingURL=RootController.js.map