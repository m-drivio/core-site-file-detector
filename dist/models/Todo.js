"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Todo = void 0;
const mongoose_1 = require("mongoose");
///
const TodoSchema = new mongoose_1.Schema({
    title: {
        type: String,
        required: true,
        unique: true,
    },
    hasCompleted: {
        type: Boolean,
        default: false,
    },
}, {
    timestamps: true,
});
exports.Todo = mongoose_1.model("Todo", TodoSchema);
//# sourceMappingURL=Todo.js.map