"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
/**
 * This User model uses passport-local-mongoose interface,
 * so please do not use this Model as reference to create other Models
 *
 * [passport-local-mongoose package home](https://www.npmjs.com/package/passport-local-mongoose)
 *
 * [Read tutorial on Local Authentication using passport](https://www.sitepoint.com/local-authentication-using-passport-node-js/)
 *
 * [Validator](https://www.npmjs.com/package/validator)
 *
 * @packageDocumentation
 */
const mongoose_1 = require("mongoose");
const validator_1 = __importDefault(require("validator"));
const passport_local_mongoose_1 = __importDefault(require("passport-local-mongoose"));
const UserSchema = new mongoose_1.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
    },
    active: {
        type: Boolean,
        default: true,
    },
    profile: {
        id: String,
        access_token: String,
        refresh_token: String,
        provider: String,
        emails: String,
    },
}, {
    timestamps: true,
});
// will simplify the integration between Mongoose and Passport
// for local authentication.It will add a hash and salt field
// to our Schema in order to store the hashed password and the salt value.
// https://www.npmjs.com/package/passport-local-mongoose
// Tutorial https://www.sitepoint.com/local-authentication-using-passport-node-js/
UserSchema.plugin(passport_local_mongoose_1.default);
/*
 * Validation
 */
UserSchema.path("username").validate(function (v) {
    return validator_1.default.isEmail(v);
}, "{VALUE} is not valid email address");
exports.User = mongoose_1.model("User", UserSchema);
//# sourceMappingURL=User.js.map