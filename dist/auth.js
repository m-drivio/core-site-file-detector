"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.enableAuth = void 0;
const Auth = __importStar(require("./lib/Auth"));
const passport_1 = __importDefault(require("passport"));
const beforeAllRoute_1 = require("./middlewares/beforeAllRoute");
/**
 * Enables authentication to the application
 * when call
 *
 * How to use?
 *
 * ```typescript
 * import * as auth from "./auth";
 * //call the function
 * auth.enableAuth(app);
 */
exports.enableAuth = function enableAuth(app) {
    /*
     * Passport Local Strategy
     * User can login with username and password
     */
    Auth.local();
    // Facebook Login
    Auth.Facebook.setStrategy(process.env.FACEBOOK_CLIENT_ID, process.env.FACEBOOK_CLIENT_SECRET, process.env.FACEBOOK_CALLBACK_URL);
    // Google Login
    Auth.Google.setStrategy(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET, process.env.GOOGLE_CALLBACK_URL);
    // Initialise passport
    app.use(passport_1.default.initialize());
    app.use(passport_1.default.session());
    // Add middleware before all route
    app.all("*", beforeAllRoute_1.beforeAllRoute);
    // Google and Facebook Auth routes
    Auth.Google.setRoute(app);
    Auth.Facebook.setRoute(app);
};
//# sourceMappingURL=auth.js.map