// @ts-nocheck
var msgEl = $("#message");
var processing = $("#submitBtn");
var submitBtn = $("#submitBtn");

$("#detectForm").submit(function (e) {
  e.preventDefault();
  msgEl.addClass("d-none");
  processing.addClass("d-none");
  submitBtn.addClass("d-none");
  var fd = new FormData();
  // @ts-ignore
  var files = $("#file")[0].files[0];
  fd.append("coverImg", files);

  $.ajax({
    url: "check",
    type: "post",
    data: fd,
    contentType: false,
    processData: false,
    success: onSuccess,
    error: onError,
    complete: onComplete,
  });
});

function onSuccess(response) {
  if (response.message == "found") {
    msgEl.html("Page detected: <b>" + response.pageName + "</b>");
  } else {
    msgEl.html("Error: " + response.message);
  }
  msgEl.removeClass("d-none alert-danger").addClass("alert-success");
}

function onError(error) {
  msgEl.html("Error: " + response.message);
  msgEl.removeClass("d-none alert-success").addClass("alert-danger");
  console.log("ERROR: " + error);
}

function onComplete() {
  processing.removeClass("d-none");
  submitBtn.removeClass("d-none");
}
