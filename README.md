# CORE Diff

The CORE Diff is a NodeJs application to find differences in the CORE's latest web pages.
It has both web view and CLI.

**Web view widget** displays new changes in a file (Uploaded)

**CLI** returns textual result.

## Installation

Clone the [repository](https://bitbucket.org/finenyc/diff/src)

```bash
# https
git clone https://akash8drivio@bitbucket.org/finenyc/diff.git
# ssh
git clone git@bitbucket.org:finenyc/diff.git
```

## Usage

@todo

## Resources

- [Jira Project](https://drivio.atlassian.net/secure/RapidBoard.jspa?rapidView=49&projectKey=FN)
- [Confluence](https://drivio.atlassian.net/wiki/spaces/FIN/overview)
- [Bitbucket](https://bitbucket.org/finenyc/diff/src)
