import * as Auth from "./lib/Auth";
import passport from "passport";
import { Application } from "express";
import { beforeAllRoute } from "./middlewares/beforeAllRoute";
/**
 * Enables authentication to the application
 * when call
 *
 * How to use?
 *
 * ```typescript
 * import * as auth from "./auth";
 * //call the function
 * auth.enableAuth(app);
 */
export const enableAuth = function enableAuth(app: Application) {
  /*
   * Passport Local Strategy
   * User can login with username and password
   */
  Auth.local();
  // Facebook Login
  Auth.Facebook.setStrategy(
    process.env.FACEBOOK_CLIENT_ID as string,
    process.env.FACEBOOK_CLIENT_SECRET as string,
    process.env.FACEBOOK_CALLBACK_URL as string
  );

  // Google Login
  Auth.Google.setStrategy(
    process.env.GOOGLE_CLIENT_ID as string,
    process.env.GOOGLE_CLIENT_SECRET as string,
    process.env.GOOGLE_CALLBACK_URL as string
  );

  // Initialise passport
  app.use(passport.initialize());
  app.use(passport.session());

  // Add middleware before all route
  app.all("*", beforeAllRoute);

  // Google and Facebook Auth routes
  Auth.Google.setRoute(app);
  Auth.Facebook.setRoute(app);
};
