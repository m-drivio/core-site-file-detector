export default interface IHtmlElementAttributeResult {
  status: string;
  children?: string[];
  report?: string;
}
