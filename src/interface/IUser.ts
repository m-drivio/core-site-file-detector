import { PassportLocalDocument } from "mongoose";

export interface IProfile {
  id: string;
  access_token: string;
  refresh_token: string;
  provider: string;
  emails?: Array<{ value: string; type?: string }>;
}

export interface IUser {
  username?: string;
  active?: boolean;
  salt?: string;
  hash?: string;
  createdAt?: Date;
  updatedAt?: Date;
  profile?: IProfile;
}

export interface IUserModel extends IUser, PassportLocalDocument {}
