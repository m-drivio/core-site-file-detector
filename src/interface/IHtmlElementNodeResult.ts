export default interface IHtmlElementNodeResult {
  status: string;
  children?: string[];
  report?: string;
}
