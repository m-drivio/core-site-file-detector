export default interface IHtmlElementAttributeCheerioObject {
  [attr: string]: string;
}
