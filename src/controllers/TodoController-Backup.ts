import { NextFunction, Request, Response } from "express";
import {
  Controller,
  IRouteParams,
  Post,
  Get,
  Middleware,
} from "@softcripto/express";
import { Todo } from "../models";
// import { TodoService } from "../services/TodoService";
import { upload } from "../middlewares/multerConfig";
import { ITodo } from "../interface";
// import { ensureLoggedIn } from "connect-ensure-login";
import Receipt from "../services/matcher/receipt";
import cheerio from "cheerio";
import getContent from "../services/get-content";

const { promisify } = require("util"),
  exec = promisify(require("child_process").exec);

interface IRequestTodoForm extends Request {
  body: ITodo;
}

// @Controller('/todos')
@Controller("/check")
export default class TodoController {
  static children: Array<IRouteParams> = []; /// required
  /**
   * List all todos.
   */
  @Middleware(upload.single("coverImg"))
  @Post("/")
  async index(req: Request, res: Response, next: NextFunction) {
    // console.log("Hell");
    // const file1 = getContent("data/receipt2.html");
    // const file2 = getContent("data/receipt3.html");
    let filePath1 = `data/${req.body.pageName}.html`;
    let filePath2 = req.file.path;
    console.log("Page name", req.body.pageName);
    if (req.body.pageName == "receipt") {
      const file1 = getContent(filePath1);
      const file2 = getContent(req.file.path);
      const receipt = new Receipt(file1, file2);
      await receipt.init();
      filePath1 = `temp/receipt1.html`;
      filePath2 = `temp/receipt2.html`;
    }
    const diffFilePath = "./result/index.html";
    const message = { status: "", resultFile: "", msg: "" };
    try {
      const html2Diff = " | diff2html -i stdin -F ";
      const command =
        "diff -u " +
        filePath1 +
        " " +
        filePath2 +
        html2Diff +
        diffFilePath +
        " -s side";
      await exec(command, {
        maxBuffer: 1024 * 1024,
      });
      let resultFile: any = getContent("./result/index.html");
      resultFile = cheerio.load(resultFile)(".d2h-wrapper").html();
      message["status"] = "changes";
      message["resultFile"] = resultFile;
      console.log("SUCCESS");
      res.send(message);
    } catch (error) {
      const str1 = "The input is empty. Try again";
      const str2 = "No such file or directory";
      if (error.stderr.search(str1) !== -1) {
        message["msg"] = "Both files are matched";
        message["status"] = "success";
      }
      if (error.stderr.search(str2) != -1) {
        message["msg"] = "something went wrong!";
        message["status"] = "error";
      }
      console.log("ERROR", error);
      res.send(message);
    }
    // console.log("RESULT==>", result);
  }
  /**
   * Render Form to create new todo.
   */
  // @Get("/create")
  // async createForm(req: Request, res: Response, next: NextFunction) {
  //   try {
  //     res.locals.errorMessage = req.flash("error");
  //     res.render("todos/create");
  //   } catch (error) {
  //     next(error);
  //   }
  // }

  /**
   * Handles New Todo
   *
   */
  @Post("/create")
  async create(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const exists = await Todo.findOne({ title: req.body.title });
      if (exists) {
        req.flash("error", "Same title already exists.");
        res.redirect(req.originalUrl);
        return;
      }
      await Todo.create(req.body);
      req.flash("success", "New Task created successfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }

  @Get("/delete/:id")
  async removeTodo(req: Request, res: Response, next: NextFunction) {
    try {
      const exists = await Todo.findById(req.params.id);
      if (!exists) {
        req.flash("error", "Invalid todo. Could not be removed.");
        res.redirect("/todos");
        return;
      }
      await Todo.remove({ _id: req.params.id });

      req.flash("success", "Task deleted succuessfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }
  /**
   * Update a Todo
   *
   */
  @Get("/update/:id")
  async updateForm(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        req.flash("error", "Invalid todo id.");
        res.redirect("/todos");
        return;
      }
      res.locals.todo = todo;
      res.render("todos/edit");
    } catch (error) {
      next(error);
    }
  }
  /**
   * Update
   */
  @Post("/update/:id")
  async update(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        req.flash("error", "Invalid todo id.");
        res.redirect("/todos");
        return;
      }
      await todo.update({ title: req.body.title });
      req.flash("success", "Task updated successfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }
}
