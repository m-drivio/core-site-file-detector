import { NextFunction, Request, Response } from "express";
import {
  Controller,
  IRouteParams,
  Post,
  Get,
  Middleware,
} from "@softcripto/express";
import { Todo } from "../models";
import cheerio from "cheerio";
// import { TodoService } from "../services/TodoService";
import { upload } from "../middlewares/multerConfig";
import { ITodo } from "../interface";
import getContent from "../services/get-content";
// import { ensureLoggedIn } from "connect-ensure-login";

interface IRequestTodoForm extends Request {
  body: ITodo;
}

// @Controller('/todos')
@Controller("/check")
export default class TodoController {
  static children: Array<IRouteParams> = []; /// required
  /**
   * List all todos.
   */
  @Middleware(upload.single("coverImg"))
  @Post("/")
  async index(req: Request, res: Response, next: NextFunction) {
    // console.log("Hell");
    const result = { status: "fail", message: "", pageName: "" };
    try {
      let file = getContent(req.file.path);
      file = file.replace(/<\!--.*?-->/g, "");
      let selectors = [ 
        { home: process.env.HOME_PAGE_SELECTOR },
        { ticket: process.env.TICKET_PAGE_SELECTOR },
        { checkout: process.env.CHECKOUT_PAGE_SELECTOR },
        { receipt: process.env.RECEIPT_PAGE_SELECTOR },
        { windshield: process.env.WINDSHIELD_PAGE_SELECTOR },
        { confirmation: process.env.CONFIRMATION_PAGE_SELECTOR },
        { maintenance: process.env.MAINTENANCE_PAGE_SELECTOR },
      ]; 
      let i = 0; 
      const selectorLength = selectors.length;
      for (i; i < selectorLength; i++) {
        let searchBy = Object.values(selectors[i])[0]?.trim();
        let currentPage = Object.keys(selectors[i])[0];
        if (searchBy) {
          if (file.search(searchBy) !== -1) {
            console.log("selector", searchBy);
            result["status"] = "success";
            result["message"] = "found";
            result["pageName"] = currentPage;
            break;
          }else{
            result["message"] = "Unknown file or selector not correct";
          }
        } else {
          result["message"] = "Incorrect selector";
        }
      }
      res.send(result);
    } catch (error) {
      result["message"] = "Something went wrong. Please try again";
      res.send(result);
    }
  }
  /**
   * Render Form to create new todo.
   */
  // @Get("/create")
  // async createForm(req: Request, res: Response, next: NextFunction) {
  //   try {
  //     res.locals.errorMessage = req.flash("error");
  //     res.render("todos/create");
  //   } catch (error) {
  //     next(error);
  //   }
  // }

  /**
   * Handles New Todo
   *
   */
  @Post("/create")
  async create(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const exists = await Todo.findOne({ title: req.body.title });
      if (exists) {
        req.flash("error", "Same title already exists.");
        res.redirect(req.originalUrl);
        return;
      }
      await Todo.create(req.body);
      req.flash("success", "New Task created successfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }

  @Get("/delete/:id")
  async removeTodo(req: Request, res: Response, next: NextFunction) {
    try {
      const exists = await Todo.findById(req.params.id);
      if (!exists) {
        req.flash("error", "Invalid todo. Could not be removed.");
        res.redirect("/todos");
        return;
      }
      await Todo.remove({ _id: req.params.id });

      req.flash("success", "Task deleted succuessfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }
  /**
   * Update a Todo
   *
   */
  @Get("/update/:id")
  async updateForm(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        req.flash("error", "Invalid todo id.");
        res.redirect("/todos");
        return;
      }
      res.locals.todo = todo;
      res.render("todos/edit");
    } catch (error) {
      next(error);
    }
  }
  /**
   * Update
   */
  @Post("/update/:id")
  async update(req: IRequestTodoForm, res: Response, next: NextFunction) {
    try {
      const todo = await Todo.findById(req.params.id);
      if (!todo) {
        req.flash("error", "Invalid todo id.");
        res.redirect("/todos");
        return;
      }
      await todo.update({ title: req.body.title });
      req.flash("success", "Task updated successfully.");
      res.redirect("/todos");
    } catch (error) {
      next(error);
    }
  }
}
