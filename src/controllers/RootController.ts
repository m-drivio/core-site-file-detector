import { NextFunction, Request, Response } from "express";
import { Controller, Get, IRouteParams, Post } from "@softcripto/express";
import passport from "passport";

@Controller("/")
export default class RootController {
  static children: Array<IRouteParams> = []; /// required
  /// Home page
  @Get("/")
  async index(req: Request, res: Response, next: NextFunction) {
    try {
      res.render("root/home");
    } catch (error) {
      next(error);
    }
  }
  /// Demo contact page
  @Get("/contact")
  one(req: Request, res: Response) {
    res.render("root/contact");
  }
  /// User Login where user can enter credentials
  /// Or Use third party login Facebook, Google etc.
  @Get("/login")
  loginForm(req: Request, res: Response) {
    if (req.isAuthenticated()) {
      return res.redirect("/");
    }
    res.locals.message = req.query.info; /// query string
    res.render("user/login", { layout: "guest" });
  }

  /// Handle User Login submission
  /// Authenticate using local strategy
  @Post("/login")
  login(req: Request, res: Response, next: NextFunction) {
    passport.authenticate("local", (err, user, info) => {
      if (err) {
        return next(err);
      }

      if (!user) {
        return res.redirect("/login?info=" + info);
      }

      req.logIn(user, function (err) {
        if (err) {
          return next(err);
        }
        return res.redirect(req.session?.returnTo || "/");
      });
    })(req, res, next);
  }

  /// Logout user
  @Get("/logout")
  logout(req: Request, res: Response) {
    req.logOut();
    res.redirect("/");
  }
}
