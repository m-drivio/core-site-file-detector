import { Profile } from "passport";
import { VerifyFunction } from "passport-facebook";
const debug = require("debug")("Passport:Facebook");
import { User } from "../../models";

/// Facebook and Google Login Callback
/// Provider Login callback
export const verifyUser = async (
  accessToken: string,
  refreshToken: string,
  profile: Profile,
  done: Function
) => {
  /// Find or Create user
  try {
    const user = await User.findOne({ "profile.id": profile.id });
    /// call callback if user has already in database
    if (user) return done(null, user);
    /// No user exists, so creating new user with facebook id
    const newUser = await User.create({
      username: profile.id,
      profile: {
        id: profile.id,
        access_token: accessToken,
        refresh_token: refreshToken,
        provider: profile.provider,
        emails: profile.emails,
      },
    });
    done(null, newUser);
  } catch (error) {
    done(error);
  }
};
