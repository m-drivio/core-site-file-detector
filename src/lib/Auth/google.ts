import passport from "passport";
import google from "passport-google-oauth";
import { Application } from "express";
import { verifyUser } from "./verifyUser";

/// Set Route
export function setRoute(app: Application) {
  app.get(
    "/auth/google",
    passport.authenticate("google", {
      scope: ["https://www.googleapis.com/auth/plus.login"],
    })
  );
  app.get(
    "/callback/google",
    passport.authenticate("google", {
      successRedirect: "/",
      failureRedirect: "/login",
    })
  );
}
/// Login Strategy
export function setStrategy(
  clientID: string,
  clientSecret: string,
  callbackURL: string
) {
  passport.use(
    new google.OAuth2Strategy(
      {
        clientID,
        clientSecret,
        callbackURL,
      },
      verifyUser
    )
  );
}
