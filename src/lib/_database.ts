import mongoose from "mongoose";
import bluebird from "bluebird";

export default function (
  mongodbDatabaseUrl: string,
  NODE_ENV: string
): Promise<typeof mongoose> {
  /// Set promise
  mongoose.Promise = bluebird;

  /// Append _test when testing
  if (NODE_ENV === "test") {
    mongodbDatabaseUrl += "_test";
  }

  /// Connect and Return promise
  return mongoose.connect(mongodbDatabaseUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  });
}
