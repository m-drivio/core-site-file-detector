import { NextFunction, Request, Response } from "express";
import { Controller, Get, IRouteParams, Post } from "@softcripto/express";
import { Todo } from "../models";
import { IPostParamsTodo } from "../interface";

@Controller("/api/todos")
export default class APITodoController {
  static children: Array<IRouteParams> = []; /// required

  @Get("/")
  async index(req: Request, res: Response, next: NextFunction) {
    try {
      const todos = await Todo.find().sort("-createdAt");
      res.json(todos);
    } catch (error) {
      next(error);
    }
  }
  @Post("/")
  async create(req: IPostParamsTodo, res: Response, next: NextFunction) {
    try {
      const todo = await Todo.create(req.body);
      res.json(todo);
    } catch (error) {
      next(error);
    }
  }
  update() {}
  remove() {}
}
