import { Request } from "express";
import multer from "multer";
export const upload = multer({
  limits: {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    fileSize: +process.env.FILE_SIZE!,
  },
  fileFilter(req: Request, file, cb) {
    // if (
    //   !file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|svg|SVG|pdf|PDF)$/)
    // ) {
    //   return cb(new Error("Please upload an image."));
    // }
    cb(null, true);
  },
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      cb(null, process.env.DEST_FOLDER!);
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    },
  }),
});
