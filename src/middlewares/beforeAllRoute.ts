import { RequestHandler } from "express";

/**
 * This Middleware executes before all Routes
 * Use this middleware to set data based on requirement
 *
 * Below I have set isAuthenticated data so it can be accessed
 * in template files to hide and show login button
 */
export const beforeAllRoute: RequestHandler = function (req, res, next) {
  res.locals.isAuthenticated = req.isAuthenticated();
  next();
};
