import { model, Schema } from "mongoose";
import { ITodoModel } from "../interface";

///
const TodoSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
    hasCompleted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

export const Todo = model<ITodoModel>("Todo", TodoSchema);
