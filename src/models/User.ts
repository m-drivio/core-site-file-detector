/**
 * This User model uses passport-local-mongoose interface,
 * so please do not use this Model as reference to create other Models
 *
 * [passport-local-mongoose package home](https://www.npmjs.com/package/passport-local-mongoose)
 *
 * [Read tutorial on Local Authentication using passport](https://www.sitepoint.com/local-authentication-using-passport-node-js/)
 *
 * [Validator](https://www.npmjs.com/package/validator)
 *
 * @packageDocumentation
 */
import { model, Schema, PassportLocalSchema } from "mongoose";
import validate from "validator";
import passportLocalMongoose from "passport-local-mongoose";
import { IUserModel } from "../interface";

const UserSchema: PassportLocalSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
    },
    active: {
      type: Boolean,
      default: true,
    },
    profile: {
      id: String,
      access_token: String,
      refresh_token: String,
      provider: String,
      emails: String,
    },
  },
  {
    timestamps: true,
  }
);
// will simplify the integration between Mongoose and Passport
// for local authentication.It will add a hash and salt field
// to our Schema in order to store the hashed password and the salt value.
// https://www.npmjs.com/package/passport-local-mongoose
// Tutorial https://www.sitepoint.com/local-authentication-using-passport-node-js/
UserSchema.plugin(passportLocalMongoose);

/*
 * Validation
 */
UserSchema.path("username").validate(function (v: string) {
  return validate.isEmail(v);
}, "{VALUE} is not valid email address");

export const User = model<IUserModel>("User", UserSchema);
