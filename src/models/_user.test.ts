import { expect } from "chai";
import { User } from ".";
/*
 * This file runs
 * before() and after() root level hooks.
 * before() hook connects to database
 * after() hook deletes all collections and drops database
 *
 * Important: must be imported
 */
import "./_hook.ignore";

describe("Model", function () {
  describe("User", function () {
    it("Should save a new user", async function () {
      const user = new User({ username: "test@drivio.com" });
      await user.setPassword("testtest@987");
      await user.save();

      expect(user).to.be.an("object");
      expect(user.salt).to.have.length.greaterThan(5);
      expect(user.hash).to.have.length.greaterThan(5);
      expect(user.username).equal("test@drivio.com");
      expect(user).to.have.property("profile");
    });

    it("Should not save duplicate username", async function () {
      try {
        const user = new User({ username: "test@drivio.com" });
        await user.save();
      } catch (e) {
        expect(e).to.be.ok;
        expect(e.message.indexOf("duplicate key error")).not.to.be.equal(-1);
      }
    });

    it("Should not save invalid username", async function () {
      try {
        const user = new User({ username: "akash" });
        await user.save();
      } catch (e) {
        expect(e.errors.username.message).equal(
          "akash is not valid email address"
        );
      }
    });
  });
});
