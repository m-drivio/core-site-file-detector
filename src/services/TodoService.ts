import { ITodoModel } from "../interface";

/// Todo Service for Template
export class TodoService {
  public title: string;
  public status: string;
  public createdAt: string;
  public id: string;

  constructor(todo: ITodoModel) {
    this.id = todo._id;
    this.title = todo.title;
    this.status = todo.hasCompleted ? "Completed" : "Pending";
    this.createdAt = todo.createdAt.toDateString();
  }

  public static init(todos: Array<ITodoModel>): Array<TodoService> {
    const list: Array<TodoService> = [];
    todos.forEach((todo: ITodoModel) => {
      list.push(new TodoService(todo));
    });
    return list;
  }
}
