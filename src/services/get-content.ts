import path from "path";
import fs from "fs";
import cheerio from "cheerio";

/**
 * get content
 * this function helps to get html content based on file path
 *
 * @param filePath - file directory path
 */

export default function getContent(filePath: string) {
  const htmlDocument = fs.readFileSync(path.resolve(filePath), {
    encoding: "utf8",
    flag: "r",
  });
  return htmlDocument;
  // return cheerio.load(removeSpaces(htmlDocument));
}

function removeSpaces(string: string) {
  return string
    .replace(/\n/g, "")
    .replace(/[\t ]+\</g, "<")
    .replace(/\>[\t ]+\</g, "><")
    .replace(/\>[\t ]+$/g, ">");
}
