/**
 * Main file to configure and run express server
 *
 * @packageDocumentation
 */

require("dotenv").config();
import { Server } from "@softcripto/express";
import { Application } from "express";
import { runServer } from "./lib/www";
import database from "./lib/_database";
import session from "./lib/_session";
import cors from "cors";

const debug = require("debug")("app");
const robots = require("express-robots-txt");
// import * as auth from "./auth";

// load all controllers that handles http request
import "./controllers";

debug("Database connection started.");

/*
 * Connecting to database
 * When connected configure server
 */
database(process.env.DATABASE as string, process.env.NODE_ENV as string)
  .then(() => {
    debug("Database connection success.");

    // Initialising Express App with @softcripto/express
    const app: Application = Server.create({
      views: "views",
      public: "public",

      // Run code just before Route mount
      beforeRouteInjection: function (app: Application) {
        app.use(cors());
        if (process.env.NODE_ENV != "production") {
          app.use(robots({ UserAgent: "*", Disallow: "/" }));
        }

        // Set session and flash middleware
        session(app, process.env.SESSION_SECRET as string);
        /*
         * Remove when auth required
         * // enable auth
         * auth.enableAuth(app);
         */
      },
    });

    // Run server
    runServer(app, process.env.PORT as string);
  })
  .catch((err) => {
    console.log(err);
    process.exit(1); // Doesn't run server
  });
