var NYC = NYC || {};
NYC.ListFilter = {};
/**
 * Class NYC.ListFilter
 *
 */
NYC.ListFilter = Class.extend({

    init : function init(options, elem) {
        // functionality flags
        this.isMobile = window.orientation || Modernizr.touch;
        this.isIE8 = ( $('.lt-ie9').length > 0 && $('.lt-ie8').length < 1 ) ? true : false;
        // cached jquery objects
        this.element = elem;
        this.alphaNav = this.element.find('div.module-alphabet-nav');
        this.mobileAlphaNav = this.element.find('div.mobile-alpha-nav');
        this.filterCol = this.element.find('#filter-topics-col');
        this.checkboxNav = this.element.find('#filter-topics-col ul.filter');
        this.filterAll = this.checkboxNav.find('#filter-all-checkbox');
        this.listCol = this.element.find('#filter-list-col');
        this.filterList = this.element.find('#filter-list-col ul#filter-list');
        // this.agenciesCount = $('.title-bar').find('span.agencies-count');
        this.selectedTopicsArray = [];
        this.templateData = "";

        this.options = $.extend({}, this.options, options);

        //this.infoShare = new NYC.InfoShare({});

        this.disableEmptyAlphas();
        // this.agenciesCount.text(this.filterList.find('ul.alpha-list li').length);

        this.bindEvents();
        this.loadJSTemplate();
    },

    options : {},

    //load js template
    loadJSTemplate : function() {

        var self = this;

        var jsTemplate = new NYC.LoadJSTemplate({
            url: "../../assets/home/js/templates/agencies-item-description.js",
            success : function(data) {
                var data = data.responseText;

                //remove this line when integrating
                data = data.substring(0, data.length-3);
                self.templateData = data;
            }
        });

    },

    showAllTopics : function() {
        var self = this;
        // enable all non-empty links in alphaNav
        self.alphaNav.find('li a').not('.empty').removeClass('filtered-out');
        //show all non-empty alphas
        var lis = self.filterList.children('li').not('.empty');
        lis.removeClass('filtered-out').show()
            .find('li.filtered-out').removeClass('filtered-out').show();

        lis.find('ul.alpha-list').children('li.last-visible').removeClass('last-visible');

        if( (self.isIE8 === false && !Modernizr.mq('(max-width: 768px)')) || (self.isIE8 === true && $(window).innerWidth() > 767) ) {
            self.updateLastVisible();
        }
        //update the number in subheader
        // self.agenciesCount.text(self.filterList.find('ul.alpha-list li').length);
        self.setEqualHeights();
    },	//showAllTopics


    updateFilteredTopics : function() {
        var self = this;
        var selectedArr = $(self.selectedTopicsArray);
        var alphaLists = self.filterList.children('li').not('.empty').find('ul.alpha-list');
        var totalActive = 0;

        //loop through every li in each ul.alpha-list
        alphaLists.children('li').each(function(i, val) {
            var li = $(val);
            var n = -1;
            // for each item in selectedTopicsArray topics array
            selectedArr.each(function(j, jval) {
                //checkbox values that were added to the array get compaired
                //against filterlist li's data-topic json objects
                n = $.inArray(jval, li.data('topic').topics );
                // if it matches, break out of the loop
                if( n > -1 ) { totalActive++; return false; }
            });
            //if no match, .filtered-out
            if( n < 0 ) { li.addClass('filtered-out'); }
            else {
                //otherwise make sure it's visible
                li.removeClass('filtered-out');
            }
        });

        //update the number in subheader
        // self.agenciesCount.text(totalActive);

        //loop through every ul.alpha-list
        alphaLists.each(function(i, val) {
            var list = $(val);
            var parentLi = list.closest('li');
            // if no children match the filter
            if( list.children('li').not('.filtered-out').length < 1 ) {
                //hide the whole ul.alpha-list
                parentLi.addClass('filtered-out');
                // disable corresponding links in alpha-nav
                self.alphaNav.find('li a[data-target='+parentLi.prop('id')+']').addClass('filtered-out');
            }
            else {
                //otherwise, show the ul.alpha-list
                list.closest('li').removeClass('filtered-out');

                // add class to last visible item in each alpha list
                list.children('li').removeClass('last-visible');
                list.children('li:visible').last().addClass('last-visible');
                // console.log( list.children('li:visible').last() );

                //show corresponding links in alpha-nav
                self.alphaNav.find('li a[data-target='+parentLi.prop('id')+']').removeClass('filtered-out');
            }
        });
        if( (self.isIE8 === false && !Modernizr.mq('(max-width: 768px)')) || (self.isIE8 === true && $(window).innerWidth() > 767) ) {
            self.updateLastVisible();
        }
        self.setEqualHeights();
    },	//updateFilteredTopics






    bindEvents : function() {
        var self = this;
        var win = $(window);

        //prevent click on disabled alphaNav links
        self.alphaNav.on('click', 'a.empty, a.filtered-out', function(e) {
            e.preventDefault();
        });

        // checkbox filters
        // 1. clicking all topics unchecks all other checkboxes
        // 2. clicking any other checkbox unchecks all topics
        // 3. if only one checkbox is checked, unchecking it causes all topics to be checked
        self.checkboxNav.on('click', 'input[type=checkbox]', function(e) {
            var target = $(this);
            var topic = '';
            // every checkbox except the all topics checkbox
            if(target.attr('id') !== 'filter-all-checkbox') {
                if(target[0].checked === true) {
                    topic = this.value;
                    target.closest('label').css({'font-weight': 'bold'});
                    self.filterAll.closest('label').css({'font-weight': 'normal'});
                    //uncheck the All Topics checkbox
                    self.filterAll.prop('checked', false);
                    //add checkbox value to array
                    self.selectedTopicsArray.push(topic);
                    //show/hide appropriate topics in list on right
                    self.updateFilteredTopics();
                }
                else {
                    //if nothing is checked
                    if(self.checkboxNav.find(':checked').length < 1) {
                        //check All Topics
                        self.filterAll.prop('checked', true);
                        self.checkboxNav.find('label').css({'font-weight': 'normal'});
                        self.filterAll.closest('label').css({'font-weight': 'bold'});
                        self.selectedTopicsArray = [];
                        self.showAllTopics();
                    }
                    else {
                        //if something is un-checked
                        target.closest('label').css({'font-weight': 'normal'});
                        // splice the topic from the array
                        var n = $.inArray(this.value, self.selectedTopicsArray);
                        self.selectedTopicsArray.splice(n, 1);
                        self.updateFilteredTopics();
                    }
                }
            }
            else {
                //the All Topics checkbox is checked
                self.selectedTopicsArray = [];
                // why this is false when the checkbox is checked, I don't know
                // if All Topics is checked, prevent un-checking it
                if(self.filterAll[0].checked == false) { e.preventDefault(); return; }
                // uncheck all other checkboxes when checking All Topics
                self.checkboxNav
                    .find('input[type=checkbox]')
                    .not('#filter-all-checkbox')
                    .prop('checked', false);
                self.checkboxNav.find('label').css({'font-weight': 'normal'});
                self.filterAll.closest('label').css({'font-weight': 'bold'});
                self.showAllTopics();
            }
        });	// self.checkboxNav



        if(!self.isMobile) {

            //links in the right column list
            self.filterList.on('click mouseover mouseout', 'a.name', function(e) {
                var target = $(this);
                var targetLi = target.closest('li');
                var targetLiHeight = targetLi.height();

                // only do it if it's not already activated
                if(!targetLi.hasClass('agency-info')) {
                    self.hoverTimeout;
                    switch(e.type) {
                        case 'click' :
                            clearTimeout(self.hoverTimeout);
                            self.hoverTimeout = null;
                            break;
                        case 'mouseover' :
                            if( (self.isIE8 === false && !Modernizr.mq('(max-width: 768px)')) || (self.isIE8 === true && win.innerWidth() > 767) ) {
                                //delay showing dialog
                                self.hoverTimeout = window.setTimeout(function() {
                                    targetLi.addClass('agency-info');
                                    if(targetLi.children('div.info').length < 1) {
                                        // if info-box doesn't exist yet, handlebars it in
                                        self.createInfoBox(targetLi);
                                    }
                                    else {
                                        //otherwise, just reveal it
                                        var infoDiv = targetLi.find('div.info');
                                        infoDiv.css({ 'height': '1px', 'visible': 'visible' })
                                            .show()
                                            .animate({ 'height': infoDiv.data('height') }, 200, function() { infoDiv.css({ 'height': 'auto'}); });
                                    }
                                    clearTimeout(self.hoverTimeout);
                                    self.hoverTimeout = null;
                                }, 500);
                            }
                            break;
                        case 'mouseout' :
                            if( (self.isIE8 === false && !Modernizr.mq('(max-width: 768px)')) || (self.isIE8 === true && win.innerWidth() > 768) ) {
                                window.clearTimeout(self.hoverTimeout);
                                self.hoverTimeout = null;
                            }
                            break;
                    }
                }
                self.setEqualHeights();
            });

            //close button in agency info box
            self.filterList.find('ul.alpha-list li').on('click', 'a.close-button', function(e) {
                e.preventDefault();
                var target = $(this);
                target.closest('li.agency-info')
                    .removeClass('agency-info')
                    .find('div.info')
                    .hide();
                // .animate({ 'height': '1px' }, 100, function() { $(this).hide(); });
                self.setEqualHeights();
            });

        } // !self.isMobile
        //filter buttons for mobile
        $('body').on('click', '#filter-agencies', function(e) {
            e.preventDefault();
            var target = $(this);
            target.hide();
            $('#filter-agencies-done').show();
            $('#filter-topics-col').show();
        });
        $('body').on('click', '#filter-agencies-done, #filter-agencies-done-inset', function(e) {
            e.preventDefault();
            $('#filter-agencies-done').hide();
            $('#filter-agencies').show();
            $('#filter-topics-col').hide();
            //jump to the top of the results
            window.location.hash = '';
            window.location.hash = 'filter-list-col';
        });
        $('body').on('click', '#view-all', function(e) {
            e.preventDefault();
            $('#filter-all-checkbox').trigger('click');
            //jump to the top of the results
            window.location.hash = '';
            window.location.hash = 'filter-list-col';
        });


        win.smartresize(function() {
            if( (self.isIE8 === false && Modernizr.mq('(max-width: 768px)')) || (self.isIE8 === true && win.innerWidth() < 768) ) {	//mobile size
                self.hideInfoBoxes();
                self.filterList.children('li.last-visible').removeClass('last-visible');
            }
            else if( (self.isIE8 === false && !Modernizr.mq('(max-width: 768px)')) || (self.isIE8 === true && win.innerWidth() > 767) ) {	//desktop size
                self.updateLastVisible();
            }
        });



    }, // bindEvents


    createInfoBox : function(target) {
        var self = this;
        var source   = self.templateData;
        // console.log('createInfoBox::: ', source);

        var template = Handlebars.compile(source);
        var data = {
            'description': target.data('desc'),
            'url': target.data('url')
        };

        // populate template and show it
        target.append(template(data));
        var infoDiv = target.find('div.info');
        infoDiv.css({'visible': 'hidden'});
        var h = infoDiv.height();
        infoDiv.data('height', h);
        infoDiv.css({ 'height': '1px', 'visible': 'visible' })
            .animate({ 'height': h }, 200, function() { infoDiv.css({ 'height': 'auto'}); });

        self.setShareOptionsOnAgencyInfoItems(target);
        //self.infoShare.reInit();
        self.setEqualHeights();
    },

    // Show/ Hide Social Icons in Infoboxes
    setShareOptionsOnAgencyInfoItems : function(target) {
        var data = target.data();
        var shareDiv = target.find('div.share');
        var theSharesCount = 0;
        shareDiv.html('');

        var share = '<a href="#" target="_blank"><span class="facebook_custom"></span></a>';
        share += '<a href="#" target="_blank"><span class="twitter_custom"></span></a>';
        share += '<a href="#" target="_blank"><span class="googleplus_custom"></span></a>';
        share += '<a href="#" target="_blank"><span class="tumblr_custom"></span></a>';
        //share += '<a href="#" target="_blank"><span class="email_custom"></span></a>';
        share += '<span class="label">Connect</span>';
        share += '</div>';

        shareDiv.html(share);

        if( !data.socialFacebook || data.socialFacebook.length < 1) {
            shareDiv.find('span.facebook_custom').parent().hide();
        }
        else {
            shareDiv.find('span.facebook_custom').parent().attr('href', data.socialFacebook);
            theSharesCount++;
        }
        if( !data.socialTwitter || data.socialTwitter.length < 1) {
            shareDiv.find('.twitter_custom').parent().hide();
        }
        else {
            shareDiv.find('span.twitter_custom').parent().attr('href', data.socialTwitter);
            theSharesCount++;
        }
        if( !data.socialGoogleplus || data.socialGoogleplus.length < 1) {
            shareDiv.find('span.googleplus_custom').parent().hide();
        }
        else {
            shareDiv.find('span.googleplus_custom').parent().attr('href', data.socialGoogleplus);
            theSharesCount++;
        }
        if( !data.socialTumblr || data.socialTumblr.length < 1) {
            shareDiv.find('span.tumblr_custom').parent().hide();
        }
        else {
            shareDiv.find('span.tumblr_custom').parent().attr('href', data.socialTumblr);
            theSharesCount++;
        }

        //email
        //shareDiv.find('span.email_custom').parent().attr('href', data.socialEmail);
        if (theSharesCount == 0){
            shareDiv.html('');
        }


    },


    hideInfoBoxes: function() {
        var self = this;
        self.filterList.find('li.agency-info')
            .removeClass('agency-info')
            .find('div.info')
            .hide();
        self.setEqualHeights();
    },


    disableEmptyAlphas : function() {
        var self = this;
        var lis = self.filterList.children('li');
        lis.each(function(index, val) {
            var li = $(this);
            //if there are no children of the letter list
            if( li.find('ul.alpha-list').children('li').length < 1 ) {
                //disable corresponding link in alphaNav
                self.alphaNav
                    .find('li a')
                    .filter('[data-target='+li.attr('id')+']')
                    .addClass('empty')
                    .attr('disabled', 'disabled');
                //remove the li if it's empty
                li.remove();
            }
        });
        self.setEqualHeights();
    },	//disableEmptyAlphas



    setEqualHeights : function() {
        var self = this;
        if( !Modernizr.mq('(max-width: 767px)') ) { 	//desktop
            // reset heights
            self.filterCol.css('height', 'auto');
            self.listCol.css('height', 'auto');
            if(self.filterCol.height() > self.listCol.height()) {
                self.listCol.height(self.filterCol.height());
            }
            else {
                self.filterCol.height(self.listCol.height());
            }
        }
        else {																				//mobile
            self.filterCol.css('height', 'auto');
            self.mobileAlphaNav.css('height', 'auto');
            self.listCol.css('height', 'auto');
            if(self.mobileAlphaNav.height() > self.listCol.height()) {
                self.listCol.height(self.mobileAlphaNav.height());
            }
            else {
                self.mobileAlphaNav.height(self.listCol.height());
            }
        }
    },

    updateLastVisible: function() {
        var self = this;
        if( (self.isIE8 === false && !Modernizr.mq('(max-width: 768px)')) || (self.isIE8 === true && $(window).innerWidth() > 767) ) {
            // special style for last visible list item
            self.filterList.children('li.last-visible').removeClass('last-visible');
            self.filterList.children('li').not('.filtered-out, .empty').last().addClass('last-visible');
        }
    }

});
